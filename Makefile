run:
	@java -jar apps/main/main-app/target/seminar-suite-app-0.0.1-SNAPSHOT.jar
#	@mvn spring-boot:run -pl :main-app

.PHONY : compile frontend compileall

compile:
	@mvn clean install -DskipTests -pl :main-app,!frontend -am

frontend:
	@mvn clean install -DskipTests -pl :frontend -amd

compileall:
	@mvn clean install

debug:
	@java -agentlib:jdwp=transport=dt_socket,address=5000,server=y,suspend=n -jar apps/main/main-app/target/seminar-suite-app-0.0.1-SNAPSHOT.jar
