package org.mephys.seminars.webapp.auth;

import lombok.Data;

public @Data class UserAuthentication {

    private String user;
    private String password;
    private String role;

}
