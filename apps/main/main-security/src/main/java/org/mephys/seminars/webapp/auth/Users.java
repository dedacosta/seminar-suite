package org.mephys.seminars.webapp.auth;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public @Data class Users {

   private Logger logger = LoggerFactory.getLogger(Users.class);

    private List<UserAuthentication> userAuthentications;

    static Users instance = null;

    private Users(){
        userAuthentications = new ArrayList<>();
    }

    public static Users getInstance() {
        if(instance==null){
            instance = new Users();
           URL config = Users.class.getResource("/config");
           if (config==null){
            instance.logger.warn("Problem with config");
           }
        }
        return instance;
    }

}
