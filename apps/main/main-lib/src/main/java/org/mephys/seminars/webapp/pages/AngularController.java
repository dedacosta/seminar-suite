package org.mephys.seminars.webapp.pages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/angular")

public class AngularController {
    @GetMapping
    public String home(Model model) {
        return "forward:/index.html";
    }

}