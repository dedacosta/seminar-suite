package org.mephys.seminars.webapp.pages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class MainPage {

    @GetMapping("/")
    public String index() {
        return "index";
    }

}