package org.mephys.seminars.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is an application for managing seminars.
 */
@SpringBootApplication
public class SeminarWebApplication  {
    public static void main(String[] args) throws Throwable {
        SpringApplication.run(SeminarWebApplication.class, args);
    }


}
